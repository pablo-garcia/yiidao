<?php

// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', true);
defined('YII_ENV') or define('YII_ENV', 'dev');

//registrar el cargador automatico de composer
require __DIR__ . '/../vendor/autoload.php';

//incluir el fichero de la clase Yii
require __DIR__ . '/../vendor/yiisoft/yii2/Yii.php';

//cargar la configuracion de la aplicacion
$config = require __DIR__ . '/../config/web.php';

//crear, configurar u ejecutar de la aplicacion
(new yii\web\Application($config))->run();
