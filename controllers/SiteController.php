<?php

namespace app\controllers;

use Yii;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\EntryForm;
use app\models\Ciclista;
use yii\data\Pagination;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\QueryInterface;
use yii\db\Query;

class SiteController extends Controller{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
      public function actionConsulta1a(){
          
        $posts = Yii::$app->db->createCommand('SELECT distinct edad from ciclista')->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "SELECT DISTINCT edad from ciclista",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con dao",
            "enunciado" => "listar las edades de los ciclistas repetidos",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
    }
    public function actionConsulta1(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "listar las edades de los ciclistas repetidos",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
}
      public function actionConsulta2a(){
          
        $posts = Yii::$app->db->createCommand("Select distinct edad from ciclista where nombre = 'artiach'")->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "Select distinct edad from ciclista where nombre = 'artiach'",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 2 con dao",
            "enunciado" => "listar las edades de los ciclistas de artiach",
            "sql" => "Select distinct edad from ciclista where nombre = 'artiach'",
            
        ]);
    }
    public function actionConsulta2(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach'" ),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 2 con active record",
            "enunciado" => "listar las edades de los ciclistas de artiach",
            "sql" => "Select distinct edad from ciclista where nombre = 'artiach'",
            
        ]);
}   
      public function actionConsulta3a(){
          
        $posts = Yii::$app->db->createCommand("Select distinct edad from ciclista where nomequipos = 'Amore Vita or nomequipo = 'Artiach'")->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "Select distinct edad from ciclista where nomequipos = 'Amore Vita or nomequipo = 'Artiach'",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 3 con dao",
            "enunciado" => "listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql" => "Select distinct edad from ciclista where nomequipos = 'Amore Vita or nomequipo = 'Artiach'",
            
        ]);
    }
     public function actionConsulta3(){
        
        $dataprovider = new ActiveDataProvider([
            
           'query' => Ciclista::find()->select("edad")->distinct()->where("nomequipo = 'Artiach' or nomequipo = 'Amore Vita'" ),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 3 con active record",
            "enunciado" => "listar las edades de los ciclistas de Artiach o de Amore Vita",
            "sql" => "Select distinct edad from ciclista where nomequipos = 'Amore Vita or nomequipo = 'Artiach'",
            
        ]);
} 

      public function actionConsulta4a(){
          
        $posts = Yii::$app->db->createCommand('Select distinct dorsal from ciclista where edad < 25 or edad > 30')->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "Select distinct dorsal from ciclista where edad < 25 or edad > 30",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 4 con dao",
            "enunciado" => "listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql" => "Select distinct dorsal from ciclista where edad < 25 or edad > 30",
            
        ]);
    }
    public function actionConsulta4(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("dorsal")->distinct()->where("edad < 25 or edad > 30"),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['dorsal'],
            "titulo" => "Consulta 4 con active record",
            "enunciado" => "listar los dorsales de los ciclistas cuya edad sea menor que 25 o mayor que 30",
            "sql" => "Select distinct dorsal from ciclista where edad < 25 or edad > 30",
            
        ]);
} 
      public function actionConsulta5a(){
          
        $posts = Yii::$app->db->createCommand("Select distinct dorsal from ciclista where edad between 28 and 32 and nomequipo = 'Banesto'")->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "Select distinct dorsal from ciclista where edad between 28 and 32 and nomequipo = 'Banesto'",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 5 con dao",
            "enunciado" => "listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto ",
            "sql" => "Select distinct dorsal from ciclista where edad between 28 and 32 and nomequipo = 'Banesto'",
            
        ]);
    }
    public function actionConsulta5(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("dorsal")->distinct()->where("edad between 28 and 32 and nombre = 'Banesto'"),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['dorsal'],
            "titulo" => "Consulta 5 con active record",
            "enunciado" => "listar los dorsales de los ciclistas cuya edad esté entre 28 y 32 y además que sólo sean de Banesto ",
            "sql" => "Select distinct dorsal from ciclista where edad between 28 and 32 and nomequipo = 'Banesto'",
            
        ]);
} 
      public function actionConsulta6a(){
          
        $posts = Yii::$app->db->createCommand('Select distinct nombre from ciclista where CHAR_LENGTH(nombre) > 8')->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "Select distinct nombre from ciclista where CHAR_LENGTH(nombre) > 8",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 6 con dao",
            "enunciado" => "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql" => "Select distinct nombre from ciclista where CHAR_LENGTH(nombre) > 8",
            
        ]);
    }
    public function actionConsulta6(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("nombre")->distinct()->where("CHAR_LENGTH(nombre) > 8"),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['nombre'],
            "titulo" => "Consulta 6 con active record",
            "enunciado" => "Indícame el nombre de los ciclistas que el número de caracteres del nombre sea mayor que 8",
            "sql" => "Select distinct nombre from ciclista where CHAR_LENGTH(nombre) > 8",
            
        ]);
} 
      public function actionConsulta7a(){
          
        $posts = Yii::$app->db->createCommand('select upper(nombre) as nombre_mayusculas, dorsal  from ciclista;')->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "select upper(nombre) as nombre_mayusculas, dorsal  from ciclista;",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 7 con dao",
             "enunciado" => "lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql" => "select upper(nombre) as nombre_mayusculas, dorsal  from ciclista;",
            
        ]);
    }
    public function actionConsulta7(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("upper(nombre) nombre,dorsal"),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['nombre','dorsal'],
            "titulo" => "Consulta 7 con active record",
            "enunciado" => "lístame el nombre y el dorsal de todos los ciclistas mostrando un campo nuevo denominado nombre mayúsculas que debe mostrar el nombre en mayúsculas",
            "sql" => "select upper(nombre) as nombre_mayusculas, dorsal  from ciclista;",
            
        ]);
} 
      public function actionConsulta8a(){
          
        $posts = Yii::$app->db->createCommand('SELECT distinct edad from ciclista')->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "SELECT DISTINCT edad from ciclista",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 8 con dao",
            "enunciado" => "listar las edades de los ciclistas repetidos",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
    }
    public function actionConsulta8(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("nombre")->distinct()->where(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "Listar todos los ciclistas que han ganado el maillot MGE (amarillo) en alguna etapa",
            "sql" => "Select distinct",
            
        ]);
} 
      public function actionConsulta9a(){
          
        $posts = Yii::$app->db->createCommand('select * from puerto where altura > 1500;')->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "select * from puerto where altura > 1500;",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 9 con dao",
            "enunciado" => "Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql" => "select * from puerto where altura > 1500;",
            
        ]);
    }
    public function actionConsulta9(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("consulta1",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "Listar el nombre de los puertos cuya altura sea mayor de 1500",
            "sql" => "select * from puerto where altura > 1500;",
            
        ]);
} 
      public function actionConsulta10a(){
          
        $posts = Yii::$app->db->createCommand("select dorsal from ciclista where dorsal in( select dorsal from puerto where pendiente > 8 or altura between 1800 and 3000)")->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "select dorsal from ciclista where dorsal in( select dorsal from puerto where pendiente > 8 or altura between 1800 and 3000);",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['dorsal'],
            "titulo" => "Consulta 1 con dao",
            "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql" => "select dorsal from ciclista where dorsal in( select dorsal from puerto where pendiente > 8 or altura between 1800 and 3000)",
            
        ]);
    }
    public function actionConsulta10(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("consulta1",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algun puerto cuya pendiente sea mayor que 8 o cuya altura esté entre 1800 y 3000",
            "sql" => "select dorsal from ciclista where dorsal in( select dorsal from puerto where pendiente > 8 or altura between 1800 and 3000);",
            
        ]);
} 
      public function actionConsulta11a(){
          
        $posts = Yii::$app->db->createCommand('SELECT distinct edad from ciclista')->queryScalar();
        
        $dataprovider = new SqlDataProvider([
            
            'sql' => "SELECT DISTINCT edad from ciclista",
            'totalCount' => $posts,
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
       return $this->render("resultado",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con dao",
            "enunciado" => "listar las edades de los ciclistas repetidos",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
    }
    public function actionConsulta11(){
        
        $dataprovider = new ActiveDataProvider([
            
            'query' => Ciclista::find()->select("edad")->distinct(),
            'pagination' => [
                'pagesize' => 5,
            ]
        ]);
        
       
    
        return $this->render("consulta1",[
            "resultados" => $dataprovider,
            "campos" => ['edad'],
            "titulo" => "Consulta 1 con active record",
            "enunciado" => "Listar el dorsal de los ciclistas que hayan ganado algún puerto cuya pendiente sea mayor que 8 y cuya altura esté entre 1800 y 3000",
            "sql" => "Select distinct edad from ciclista",
            
        ]);
} 

    /** 
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    
    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
            
        
        public function actionEntry() {
            
            $model = new EntryForm();
            
            if($model->load(Yii::$app->request->post()) && $model->validate()){
            //validar los datos recibidos del modelo
                
                return $this->render('entry-confirm',['model'=> $model]);
            } else {
                
                return $this->render('entry',['model' => $model]);
                
                
            }
            
        }
        
    
    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
